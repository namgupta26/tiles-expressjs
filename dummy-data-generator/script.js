//The script generates dummy data for the Tiles application by calling Node Apis, you need to have the application running

const fetch = require("node-fetch");
const config = require('../config/env.config')

apiEndpoint = config.apiEndpoint
if (config.environment == "prod"){
  apiEndpoint = config.prodApiEndpoint
}



var getAllUsers = () => {
  fetch(apiEndpoint + '/users')
    .then(response => response.json())
    .then(data => console.log(data));
}

var addUser = async (data) => {
  let response = await fetch(apiEndpoint + '/users/add', {
    method: 'POST',
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json, text/plain, */*",
    },
    body: JSON.stringify(data)
  })
  let responseData = await response.json()
}



var getFriends = (user) => {
  fetch(apiEndpoint + '/friends?user='+user)
    .then(response => response.json())
    .then(data => console.log(data));
}

var addFriend = async (data) => {
  await fetch(apiEndpoint + '/friends/add', {
    method: 'POST',
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json, text/plain, */*",
    },
    body: JSON.stringify(data)
  }).then(response => response.json())
  // .then(data => console.log(data));
}





var addPost = async (data) => {
  await fetch(apiEndpoint + '/feed/posts/add', {
    method: 'POST',
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json, text/plain, */*",
    },
    body: JSON.stringify(data)
  }).then(response => response.json())
  // .then(data => console.log(data));
}

var getPosts = (user) => {
  fetch(apiEndpoint + '/feed/posts?user='+user)
    .then(response => response.json())
    .then(data => console.log(data));
}

var getWall = (user) => {
  fetch(apiEndpoint + '/feed/wall?user='+user)
    .then(response => response.json())
    .then(data => console.log(data));
}





//users generated with usernames - User1, User2 ...
var generateUsers = async (len) => {
  let arr = []
  for (var i = 1; i <= len; i++) {
    arr.push("User"+i);
    await addUser({username:"User"+i, full_name:"FullName"+i})
  }
  return arr
}

//for every user, a set of friends are generated, the number of friends is represented by friends_to_add, random indexes from the main users array is chosen to select friends.
var generateFriends = (users) => {
  // console.log(users)
  users.forEach(async (user,index) => {
    let friends_to_add = Math.floor(Math.random() * (users.length)/2);
    
    let temp_userlist = [...users];
    temp_userlist.splice(index,1)

    //generate random sequence of friends
    while(friends_to_add>0){
      let random_index = Math.floor(Math.random() * temp_userlist.length);
      // console.log("adding Friend:  ",user,temp_userlist[random_index])
      await addFriend({user:user, friend:temp_userlist[random_index]})
      temp_userlist.splice(random_index,1)
      friends_to_add--;
    }
  });
}

//for every user generate posts, limit (1,maxPosts)
var generatePosts = async (users, maxPosts) => {
  users.forEach(async user => {
    let postCount = Math.floor(Math.random() * maxPosts) + 1;
      
    for(let i=0;i<postCount;i++) {
      let random_image_index = Math.floor(Math.random() * 16) + 1;
      await addPost({
          created_by:user, 
          image_url: apiEndpoint + "/static/feed_images/"+ random_image_index +".jpg",
          description: user+" "+random_image_index
        })
      }
  });
}



var generateData = async (maxUsers, maxPostsPerUser) => {
  console.log("Please wait while the system generates dummy data. The script will close on completion automatically")
  console.log("Generating Users")
  let users = await generateUsers(maxUsers)
  console.log("Generating Friends and Posts, this may take a while")
  generateFriends(users)
  generatePosts(users, maxPostsPerUser)

}


// addUser({username:"namangupta", full_name:"Naman"})
// addFriend({user:"namangupta", friend:"namangupta2"})
// getFriends('namangupta')
// addPost({
//   created_by:"namangupta", 
//   image_url:"http://127.0.0.1:5100/static/feed_images/"+ Math.floor(Math.random() * 11) +".jpg",
//   description:"namangupta "
// })
// getPosts('User1')
// getWall('User1')


generateData(50,20)

const UsersModel = require('./users.model');
const FriendsModel = require('../friends/friends.model');

exports.addUser = (req, res) => {
    UsersModel.addUser(req.body)
    .then(newUser => {

        // create friend list along with the user.
        FriendsModel.initializeFriendList(req.body.username)
        .then(response => { 
            res.status(200).json(newUser);
        })
        .catch(err => {
            res.status(400).send('Failed to create new user friend list' + err);
        });
    })
    .catch(err => {
        res.status(400).send('Failed to create new user' + err);
    });
        
};


exports.getUserList = (req, res) => {
    UsersModel.getUserList(req.body)
    .then(userList => {
        res.status(200).json(userList);
    })
    .catch(err => {
        res.status(400).send('Failed to create new user' + err);
    });
        
};
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;


let UserSchema = new Schema({
    username: {
        type: String,
    },
    full_name: {
        type: String
    } 
}, {
    collection: 'User',
    timestamps: true
});

const User = mongoose.model('User', UserSchema);
exports.User = User;


// method to add a new user to the social media platform.
exports.addUser = (body) => {
    return new Promise((resolve, reject) => {

        let userData = new User(body);
        userData.save((err, responseData) => {
            if (err) {
                return reject(err);
            } else {
                    resolve(responseData);
            };
        });
    });
};

// method to list all users in the social platform.
exports.getUserList = (body) => {

    return new Promise((resolve, reject) => {
        
        User.find((err, responseData) => {
            if (err) {
                return reject(err);
            } else {
                    resolve(responseData);
            };
        });
    });
};


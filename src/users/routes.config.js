const UsersController =  require('./users.controller')
const express = require('express');
const router = express.Router();

router.post('/add', [
    UsersController.addUser
]);

router.get('/', [
    UsersController.getUserList
]);

module.exports = router;
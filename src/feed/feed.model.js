const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
const Friends = require('../friends/friends.model');

let PostsSchema = new Schema({
    created_by: {
        type: String,
    },
    image_url: {
        type: String,
    },
    likes : {
        type: Number
    },
    description: {
        type: String,
    }
}, {
    collection: 'Posts',
    timestamps: true
});

const Posts = mongoose.model('Posts', PostsSchema);
exports.Posts = Posts;



exports.addPost = (body) => {
    return new Promise((resolve, reject) => {
        body.likes = Math.floor(Math.random() * 1000);
        let postData = new Posts(body);
        postData.save((err, responseData) => {
            if (err) {
                return reject(err);
            } else {
                    resolve(responseData);
            };
        });
    });
};


exports.getUserPosts = (user) => {

    return new Promise((resolve, reject) => {
        
        Posts.find({'created_by' : user}).sort('created_at').exec((err, responseData) => {
            if (err) {
                return reject(err);
            } else {
                    resolve(responseData);
            };
        });
    });
};





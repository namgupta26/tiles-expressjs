const FeedModel = require('./feed.model');  
const FriendsModel = require('../friends/friends.model');

exports.addPost = (req, res) => {

    FeedModel.addPost(req.body)
    .then(newFeed => {
        res.status(200).json(newFeed);
    })
    .catch(err => {
        res.status(400).send('Failed to add new post' + err);
    });
        
};


exports.getUserPosts = (req, res) => {

    FeedModel.getUserPosts(req.query.user)
    .then(userPosts => {
        res.status(200).json(userPosts);
    })
    .catch(err => {
        res.status(400).send('Failed to list user posts' + err);
    });
        
};


exports.getFeedWall = (req, res) => {
    
    let pageLimit = 25
    let pageNumber = req.query.page ? req.query.page : 1

    FriendsModel.getFeedWall(req.query)
    .then(feed => {
        let totalPages = Math.ceil(feed.length/pageLimit)
        //ranking and sorting algo
        feed = rankAndSort(feed)
        feed = getFeedByPage(feed, pageLimit, pageNumber)
        res.status(200).json({ feed: feed, totalPages : totalPages});
    })
    .catch(err => {
        res.status(400).send('Failed to get feed' + err);
    });
        
};

//ranking algorithm and sort
const rankAndSort = (arr) => { 

    let avg_friend_rating = 0;
    let avg_likes = 0;
    arr.forEach(element => {
        avg_likes += element.post_likes
        avg_friend_rating += element.friend_rating
    });
    avg_likes = avg_likes / arr.length
    avg_friend_rating = avg_friend_rating / arr.length

    arr.forEach(element => {
        element.score = element.post_likes / avg_likes * element.friend_rating /avg_friend_rating
    })

    arr.sort((a, b) => (a.score < b.score) ? 1 : -1)

    return arr

}

const getFeedByPage = (feed, pageSize, pageNumber) => {
    return feed.slice((pageNumber-1)*pageSize, pageNumber*pageSize)

}
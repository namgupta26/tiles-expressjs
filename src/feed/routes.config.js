const FeedController =  require('./feed.controller')
const express = require('express');
const router = express.Router();

router.post('/posts/add', [
    FeedController.addPost
]);

router.get('/posts', [
    FeedController.getUserPosts
]);

router.get('/wall', [
    FeedController.getFeedWall
]);

module.exports = router;
const FriendsModel = require('./friends.model');



exports.addFriend = (req, res) => {
    FriendsModel.addFriend(req.body)
    .then(newFriend => {
        //add to other users list as well
        FriendsModel.addFriend({user: req.body.friend, friend: req.body.user})
        .then(newFriend1 => {
            res.status(200).json(newFriend);
        })
        .catch(err => {
            res.status(400).send('Failed to add friend' + err);
        });
    })
    .catch(err => {
        res.status(400).send('Failed to add friend' + err);
    });
        
};


exports.getFriends = (req, res) => {

    FriendsModel.getFriends(req.query.user)
    .then(friendList => {
        res.status(200).json(friendList);
    })
    .catch(err => {
        res.status(400).send('Failed to get friends' + err);
    });
        
};
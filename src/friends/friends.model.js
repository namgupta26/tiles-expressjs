const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;


let FriendSchema = new mongoose.Schema({
    friend: String,
    friend_rating: String
});


let FriendListSchema = new Schema({
    user: {
        type: String,
    },
    friend_list: [FriendSchema]
}, {
    collection: 'Friends',
    timestamps: true
});

const Friends = mongoose.model('Friends', FriendListSchema);
exports.Friends = Friends;


exports.initializeFriendList = (user) => {
    return new Promise((resolve, reject) => {

        let friendData = new Friends({user: user, friend_list: []});
        friendData.save((err, responseData) => {
            if (err) {
                return reject(err);
            } else {
                    resolve(responseData);
            };
        });
        
    });
};


exports.addFriend = (body) => {
    // console.log(body + "Here")
    return new Promise((resolve, reject) => {

        Friends.findOne({'user': body.user},(err, friendListResponse) => {
            if (err) {
                return reject(err);
            } else {
                //pending = check if friend exists
                // console.log(friendListResponse)
                friendListResponse.friend_list.push({'friend': body.friend, 'friend_rating' : Math.floor(Math.random() * 100)+1})
                friendListResponse.save((err, responseData) => {
                    if (err) {
                        return reject(err);
                    } else {
                            resolve(responseData);
                    };
                });
            };
        });
        
    });
};


exports.getFriends = (user) => {

    return new Promise((resolve, reject) => {
        
        Friends.findOne({user:user},(err, responseData) => {
            if (err) {
                return reject(err);
            } else {
                    resolve(responseData.friend_list);
            };
        });
    });
};



exports.getFeedWall = (query) => {
    
    let user = query.user

    return new Promise((resolve, reject) => {
        
        Friends.aggregate(
            [{
                    '$match': {
                        'user': user
                    }
                },
                {
                    $unwind: "$friend_list"
                },
                {
                    '$lookup': {
                        from: 'Posts',
                        localField: 'friend_list.friend',
                        foreignField: 'created_by',
                        as: 'posts'
                    }
                },
                {
                    $unwind: "$posts"
                },
                {
                    $project: {
                        '_id': 0,
                        'friend': '$friend_list.friend',
                        'friend_rating': '$friend_list.friend_rating',
                        'post_image_url': '$posts.image_url',
                        'post_likes': '$posts.likes',
                        'post_description': '$posts.description',
                    }
                }
            ],
            function (err, responseData) {
                if (err)
                    reject(err);
                else {
                    resolve((responseData));
                }
            });
    });
};

const FriendsController =  require('./friends.controller')
const express = require('express');
const router = express.Router();

router.post('/add', [
    FriendsController.addFriend
]);

router.get('/', [
    FriendsController.getFriends
]);

module.exports = router;
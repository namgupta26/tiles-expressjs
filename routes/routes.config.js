const express = require('express');
const router = express.Router();

const FeedRouter = require('../src/feed/routes.config')
const FriendsRouter = require('../src/friends/routes.config')
const UsersRouter = require('../src/users/routes.config')
const AuthRouter = require('../authorization/routes.config')

router.use('/feed', FeedRouter);

router.use('/friends/', FriendsRouter);

router.use('/users', UsersRouter);

// router.use('/auth/', AuthRouter);

module.exports = router;
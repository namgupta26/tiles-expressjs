const express = require("express")
const mongoose = require("mongoose") // new
const bodyParser = require('body-parser');
const mainRouter = require('./routes/routes.config')
const config = require('./config/env.config')
const app = express();

//for conneting to localhost
app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
	res.setHeader('Access-Control-Allow-Credentials', true);
	next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/static', express.static('public'))
app.use('/', mainRouter);

// Connect to MongoDB database
mongoose
.connect("mongodb+srv://"+config.dbUsername+":"+config.dbPassword+"@cluster0.iyb13.mongodb.net/"+config.db+"?retryWrites=true&w=majority", { useNewUrlParser: true,  useUnifiedTopology: true })
.then(() => {
	app.listen(config.port, () => {
		console.log("Server has started! at port 5100")
	})
})